define([
    'knockout',
    './ko/bind/switch',
    './ko/bind/color'
], function(ko) {
    'use strict';

    function Notification(data) {
        var self = this;

        self.inherit = ko.observable(data.inherit || 0);
        self.name = ko.observable(data.name);
        self.key = ko.observable(data.key);
        self.send_type = ko.observable(data.send_type);
        self.custom_channel = ko.observable(data.custom_channel);
        self.colorize = ko.observable(+data.colorize);
        self.color = ko.observable(data.color);

        self.buildName = function(field, multi) {
            return 'groups[notifications][fields][' + self.key() + '_' + field + '][value]' + (multi ? '[]' : '');
        };

        self.buildInheritName = function(field) {
            return 'groups[notifications][fields][' + self.key() + '_' + field + '][inherit]';
        };
    }

    function NotificationsList(data) {
        var self = this;
        self.show_inherit_checkbox = ko.observable(data.show_inherit_checkbox || false);
        self.inherit_label = ko.observable(data.inherit_label || false);

        self.notificationsData = ko.observableArray();
        ko.utils.arrayForEach(data.list, function(d) {
            self.notificationsData.push(new Notification(d));
        });
        self.getTemplate = function() {
            return 'Moogento_SlackCommerce/notifications';
        };
    }

    return function(data) {
        return new NotificationsList(data);
    };
});
