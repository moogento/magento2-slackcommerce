<?php

namespace Moogento\SlackCommerce\Plugin\Adminhtml;

use Magento\Backend\Model\Menu;
use Magento\Backend\Model\Menu\Builder;
use Magento\Backend\Model\Menu\Item\Factory as MenuItemFactory;
use Moogento\SlackCommerce\Helper\Data;

class AddDynamicMenu
{
    /**
     * @var MenuItemFactory
     */
    protected $menuItemFactory;

    /**
     * @var Data
     */
    protected $helper;

    /**
     * AddDynamicMenu constructor.
     * @param MenuItemFactory $menuItemFactory
     * @param Data $helper
     */
    public function __construct(
        MenuItemFactory $menuItemFactory,
        Data $helper
    ) {
        $this->menuItemFactory = $menuItemFactory;
        $this->helper = $helper;
    }

    /**
     * @param Builder $subject
     * @param Menu $menuModel
     * @return Menu
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function afterGetResult(Builder $subject, Menu $menuModel)
    {
        if (!$this->helper->isModuleOutputEnabled('Moogento_License')) {
            return $menuModel;
        }

        $item = $this->menuItemFactory->create([
            'id' => 'Moogento_SlackCommerce::menu',
            'title' => 'SlackCommerce',
            'resource' => 'Moogento_SlackCommerce::menu',
            'action' => null,
        ]);
        $menuModel->add($item, 'Moogento_License::menu', 600);

        $item = $this->menuItemFactory->create([
            'id' => 'Moogento_SlackCommerce::menu_configuration',
            'title' => 'Config.',
            'resource' => 'Moogento_SlackCommerce::menu',
            'action' => 'adminhtml/system_config/edit/section/moogento_slackcommerce',
        ]);
        $menuModel->add($item, 'Moogento_SlackCommerce::menu', 120);

        $item = $this->menuItemFactory->create([
            'id' => 'Moogento_SlackCommerce::menu_guide',
            'title' => 'Guide',
            'resource' => 'Moogento_SlackCommerce::menu_guide',
            'action' => 'slackcommerce/usage/',
        ]);
        $menuModel->add($item, 'Moogento_SlackCommerce::menu', 125);
        return $menuModel;
    }
}
