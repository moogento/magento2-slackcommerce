<?php
namespace Moogento\SlackCommerce\Setup;

use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Module\Dir;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Module\Dir\Reader;
use Magento\Framework\Filesystem;

class InstallData implements InstallDataInterface
{
    public $moduleReader;
    public $mediaDirectory;
    /** @var  \Magento\Framework\Filesystem\Directory\ReadFactory */
    public $readFactory;

    public function __construct(
        Reader $moduleReader,
        Filesystem $filesystem,
        \Magento\Framework\Filesystem\Directory\ReadFactory $readFactory
    ) {
        $this->moduleReader = $moduleReader;
        $this->mediaDirectory = $filesystem->getDirectoryWrite(
            DirectoryList::MEDIA
        );
        $this->readFactory = $readFactory;
    }

    /**
     * Installs data for a module
     *
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface   $context
     *
     * @return void
     */
    public function install(
        ModuleDataSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        if ($setup && $context) {
            if (!$this->mediaDirectory->isExist(
                'moogento/slack/moogento_logo_small.png'
            )
            ) {
                $this->mediaDirectory->writeFile(
                    'moogento/slack/moogento_logo_small.png',
                    $this->readFactory->create(
                        $this->moduleReader->getModuleDir(
                            Dir::MODULE_VIEW_DIR,
                            'Moogento_SlackCommerce'
                        )
                    )->readFile('moogento_logo_small.png')
                );
            }
        }
    }
}
