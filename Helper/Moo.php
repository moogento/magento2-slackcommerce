<?php
namespace Moogento\SlackCommerce\Helper;

use Magento\Backend\Model\Auth\Session;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\App\ProductMetadataInterface;
use Magento\Framework\Module\DbVersionInfo;
use Magento\Framework\Module\Dir;
use Magento\Framework\Module\Dir\Reader;
use Magento\Framework\Module\ResourceInterface;

class Moo extends AbstractHelper
{
    public $module = 'Moogento_SlackCommerce';

    public $moduleReader;
    public $moduleResource;
    public $authSession;

    /** @var  \Magento\Framework\Json\Helper\Data */
    public $jsonHelper;

    public function __construct(
        Context $context,
        Reader $moduleReader,
        ResourceInterface $moduleResource,
        Session $authSession,
        \Magento\Framework\Json\Helper\Data $jsonHelper
    ) {
        $this->moduleReader = $moduleReader;
        $this->moduleResource = $moduleResource;
        $this->authSession = $authSession;
        $this->jsonHelper = $jsonHelper;
        
        parent::__construct($context);
    }

    public function x()
    {
        $d = [
            '',
            '',
            '',
            '',
            'slackcommerce',
            $this->v(),
            '',
            '',
            'M2',
        ];
        return implode('||', $d);
    }

    //info
    public function i()
    {
        return base64_encode(base64_encode($this->x()));
    }

    //logo
    public function l()
    {
        return base64_encode(
            base64_encode(base64_encode(base64_encode($this->x())))
        );
    }

    public function v()
    {
        $composerFile = $this->moduleReader->getComposerJsonFiles();

        if ($composerFile->current()) {
            $composerConfig = $this->jsonHelper->jsonDecode(
                $composerFile->current()
            );
            if ($composerConfig && isset($composerConfig['version'])) {
                return $composerConfig['version'];
            } else {
                return $this->moduleResource->getDbVersion($this->module);
            }
        } else {
            return $this->moduleResource->getDbVersion($this->module);
        }
    }
}
