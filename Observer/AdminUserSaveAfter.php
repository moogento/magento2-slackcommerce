<?php
namespace Moogento\SlackCommerce\Observer;

use Magento\Framework\Event\Observer;
use Moogento\SlackCommerce\Helper\Config;

class AdminUserSaveAfter extends ObserverAbstract
{
    /**
     * @param Observer $observer
     *
     * @return void
     */
    public function execute(Observer $observer)
    {
        /** @var \Magento\User\Model\User $user */
        $user = $observer->getDataObject();

        if ($user->getData(Config::NOTIFICATION_FLAG)
            && $this->configHelper->shouldSend(
                \Moogento\SlackCommerce\Model\Queue::KEY_NEW_BACKEND_ACCOUNT
            )
        ) {
            $user->setData(Config::NOTIFICATION_FLAG, false);

            $queue = $this->queueFactory->create();
            $queue->setData(
                [
                    'event_key' =>
                        \Moogento\SlackCommerce\Model\Queue::
                        KEY_NEW_BACKEND_ACCOUNT,
                    'reference_id' => $user->getId(),
                    'date' => $this->dateTime->gmtDate("Y-m-d H:i:s"),
                ]
            );
            try {
                $queue->save();
            } catch (\Exception $e) {
                $this->logger->warning($e);
            }
        }
    }
}
