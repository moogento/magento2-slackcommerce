<?php
namespace Moogento\SlackCommerce\Cron;

use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\UrlInterface;
use Moogento\SlackCommerce\Helper\Config;

class CronAbstract
{
    /** @var CronContext */
    protected $_context;

    public function __construct(CronContext $context)
    {
        $this->_context = $context;
    }

    protected function _getMagentoDate()
    {
        return $this->_context->getDateTime()->date('l jS M');
    }

    protected function _getGeneralData()
    {
        $data = [
            'channel'     => null,
            'attachments' => [],
        ];

        if ($this->_context->getConfigHelper()
                ->getSendType(Config::KEY_STATS, Config::SECTION_STATS)
            == Config::SEND_TYPE_CUSTOM
        ) {
            $data['channel'] = $this->_context->getConfigHelper()->getValue(
                Config::KEY_STATS,
                Config::SUBTYPE_CUSTOM_CHANNEL,
                Config::SECTION_STATS
            );
        }

        $store = $this->_context->getStoreManager()->getDefaultStoreView();
        //Start environment emulation of the specified store
        $this->_context->getAppEmulation()->startEnvironmentEmulation(
            $store->getId()
        );

        $data['username'] = $store->getName();
        $mediaDirectory   = $this->_context->getFileSystem()->getDirectoryRead(
            DirectoryList::MEDIA
        );
        $icon             = $this->_context->getConfigHelper()->getValue(
            Config::KEY_ICON,
            Config::SECTION_GENERAL
        );
        $iconUrl
                          =
            $store->getBaseUrl(UrlInterface::URL_TYPE_MEDIA) . 'moogento/slack/'
            . $icon;
        if ($icon && $mediaDirectory->isExist('moogento/slack/' . $icon)) {
            $data['icon_url'] = $iconUrl;
        } else {
            $data['icon_url']
                = $store->getBaseUrl(UrlInterface::URL_TYPE_MEDIA)
                  . 'moogento/slack/moogento_logo_small.png';
        }

        $this->_context->getAppEmulation()->stopEnvironmentEmulation();

        return $data;
    }

    protected function _getIfNullSql($expression, $value = 0)
    {
        if ($expression instanceof \Zend_Db_Expr
            || $expression instanceof \Zend_Db_Select
        ) {
            $expression = sprintf("IFNULL((%s), %s)", $expression, $value);
        } else {
            $expression = sprintf("IFNULL(%s, %s)", $expression, $value);
        }

        return new \Zend_Db_Expr($expression);
    }
}
