<?php
namespace Moogento\SlackCommerce\Model\Notification;

use Moogento\SlackCommerce\Model\NotificationAbstract;

class BackendLogin extends NotificationAbstract
{
    protected $_referenceModel = '\Magento\User\Model\User';

    protected function _prepareText()
    {
        return __('Backend login');
    }

    protected function _getAttachments()
    {
        return [
            'fields' => [
                [
                    'title' => __('User'),
                    'value' => $this->_getReferenceObject()->getUsername(),
                    'short' => true,
                ],
            ],
        ];
    }
}
