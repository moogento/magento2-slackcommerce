<?php
namespace Moogento\SlackCommerce\Observer\SalesOrder;

use Magento\Framework\Event\Observer;
use Moogento\SlackCommerce\Helper\Config;
use Moogento\SlackCommerce\Observer\ObserverAbstract;

class ShipmentSaveAfter extends ObserverAbstract
{
    /**
     * @param Observer $observer
     *
     * @return void
     */
    public function execute(Observer $observer)
    {
        /** @var \Magento\Sales\Model\Order\Shipment $shipment */
        $shipment = $observer->getShipment();

        if ($shipment->getData(Config::NOTIFICATION_FLAG)
            && $this->configHelper->shouldSend(
                \Moogento\SlackCommerce\Model\Queue::KEY_NEW_SHIPMENT
            )
        ) {
            $shipment->setData(Config::NOTIFICATION_FLAG, false);

            $queue = $this->queueFactory->create();
            $queue->setData(
                [
                    'event_key' =>
                        \Moogento\SlackCommerce\Model\Queue::KEY_NEW_SHIPMENT,
                    'reference_id' => $shipment->getId(),
                    'date' => $this->dateTime->gmtDate("Y-m-d H:i:s"),
                ]
            );
            try {
                $queue->save();
            } catch (\Exception $e) {
                $this->logger->warning($e);
            }
        }
    }
}
