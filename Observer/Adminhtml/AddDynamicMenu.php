<?php

namespace Moogento\SlackCommerce\Observer\Adminhtml;

use Magento\Framework\Event\ObserverInterface;
use Moogento\SlackCommerce\Helper\Data;

class AddDynamicMenu implements ObserverInterface
{
    /**
     * @var \Magento\Backend\Model\Menu\Item\Factory
     */
    protected $menuItemFactory;

    /**
     * @var Data
     */
    protected $helper;

    /**
     * AddDynamicMenu constructor.
     * @param \Magento\Backend\Model\Menu\Item\Factory $menuItemFactory
     * @param Data $helper
     */
    public function __construct(
        \Magento\Backend\Model\Menu\Item\Factory $menuItemFactory,
        Data $helper
    ) {
        $this->menuItemFactory = $menuItemFactory;
        $this->helper = $helper;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        if (!$this->helper->isModuleOutputEnabled('Moogento_License')) {
            return $this;
        }
        $block = $observer->getBlock();

        if ($block instanceof \Magento\Backend\Block\Menu) {
            $menuModel = $block->getMenuModel();
            $item = $this->menuItemFactory->create([
                'id' => 'Moogento_SlackCommerce::menu',
                'title' => 'SlackCommerce',
                'resource' => 'Moogento_SlackCommerce::menu',
                'action' => null,
            ]);
            $menuModel->add($item, 'Moogento_License::menu', 600);

            $item = $this->menuItemFactory->create([
                'id' => 'Moogento_SlackCommerce::menu_configuration',
                'title' => 'Config.',
                'resource' => 'Moogento_SlackCommerce::menu',
                'action' => 'adminhtml/system_config/edit/section/moogento_slackcommerce',
            ]);
            $menuModel->add($item, 'Moogento_SlackCommerce::menu', 120);

            $item = $this->menuItemFactory->create([
                'id' => 'Moogento_SlackCommerce::menu_guide',
                'title' => 'Guide',
                'resource' => 'Moogento_SlackCommerce::menu_guide',
                'action' => 'slackcommerce/usage/',
            ]);
            $menuModel->add($item, 'Moogento_SlackCommerce::menu', 125);
        }
    }
}