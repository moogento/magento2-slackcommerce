<?php
namespace Moogento\SlackCommerce\Block\Adminhtml\System\Config;

use Magento\Backend\Block\Template;
use Magento\Backend\Block\Template\Context;
use Magento\Framework\Data\Form\Element\AbstractElement;
use Magento\Framework\Data\Form\Element\Renderer\RendererInterface;
use Moogento\SlackCommerce\Helper\Moo;

/**
 * @method Hint setElement
 */
class Hint extends Template implements RendererInterface
{
    protected $_template = 'Moogento_SlackCommerce::system/config/hint.phtml';

    protected $_helper;

    public function __construct(Context $context, Moo $helper, array $data = [])
    {
        $this->_helper = $helper;
        parent::__construct($context, $data);
    }

    /**
     * Render form element as HTML
     *
     * @param AbstractElement $element
     *
     * @return string
     */
    public function render(AbstractElement $element)
    {
        $this->setElement($element);
        return $this->toHtml();
    }

    public function getLogo()
    {
        return $this->_scopeConfig->getValue('moogento/logo/url')
               . 'media/moo_logo/' . $this->_helper->l()
               . '/moogento_logo_slackcommerce.png';
    }

    public function getInfo()
    {
        return $this->_scopeConfig->getValue('moogento/logo/url')
               . 'media/moo_info/' . $this->_helper->i()
               . '/moogento_slackcommerce.js';
    }
}
