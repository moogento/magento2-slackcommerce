<?php
namespace Moogento\SlackCommerce\Observer;

use Magento\Framework\Event\Observer;
use Moogento\SlackCommerce\Helper\Config;

class BackendLoginFail extends ObserverAbstract
{
    /**
     * @param Observer $observer
     *
     * @return void
     */
    public function execute(Observer $observer)
    {
        $ip        = $this->configHelper
            ->getRemoteAddress()->getRemoteAddress();
        $urlTarget = $this->request->getDistroBaseUrl()
                     . $this->request->getOriginalPathInfo();
        $urlTarget = preg_replace('|/key/([^/]*)|i', '', $urlTarget);
        $urlTarget = str_replace('//', '/', $urlTarget);

        $ipTableName = $this->resource->getTableName(
            'moogento_slackcommerce_fails_ip'
        );
        $data        = [
            'ip'            => new \Zend_Db_Expr('INET6_ATON("' . $ip . '")'),
            'fails'         => 1,
            'fails_per_day' => 1,
        ];
        $updateData  = [
            'fails'         => new \Zend_Db_Expr('fails+1'),
            'fails_per_day' => new \Zend_Db_Expr('fails_per_day+1'),
        ];
        $this->connection->insertOnDuplicate($ipTableName, $data, $updateData);

        $targetTableName = $this->resource->getTableName(
            'moogento_slackcommerce_fails_target'
        );
        $data            = [
            'target'        => $urlTarget,
            'fails_per_day' => 1,
        ];
        $updateData      = [
            'fails_per_day' => new \Zend_Db_Expr('fails_per_day+1'),
        ];
        $this->connection->insertOnDuplicate(
            $targetTableName,
            $data,
            $updateData
        );

        if ($this->configHelper->shouldSend(
            Config::KEY_IMMEDIATE,
            Config::SECTION_SECURITY
        )
        ) {
            $message  = $observer->getException()->getMessage();
            $userName = $observer->getUserName();

            $queue = $this->queueFactory->create();
            $event_key =  \Moogento\SlackCommerce\Model\Queue::
            KEY_BACKEND_LOGIN_FAIL;
            $queue->setData(
                [
                    'event_key'       => $event_key,
                    'reference_id'    => 0,
                    'date'            => $this->dateTime->gmtDate(
                        "Y-m-d H:i:s"
                    ),
                    'additional_data' => [
                        'username' => $userName,
                        'message'  => $message,
                        'IP'       => $ip,
                        'URL'      => $urlTarget,
                    ],
                ]
            );
            try {
                $queue->save();
            } catch (\Exception $e) {
                $this->logger->warning($e);
            }
        }
    }
}
