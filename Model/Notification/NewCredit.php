<?php
namespace Moogento\SlackCommerce\Model\Notification;

class NewCredit extends NewOrder
{
    protected $_referenceModel = '\Magento\Sales\Model\Order\Creditmemo';

    protected function _getOrder()
    {
        return $this->_getReferenceObject()->getOrder();
    }

    protected function _prepareText()
    {
        return __(
            'Creditmemo (Order #%1)',
            $this->_getOrder()->getIncrementId()
        );
    }

    protected function _getAttachments()
    {
        return [
            'fields' => array_merge(
                [
                    [
                        'title' => __('Credit Amount'),
                        'value' => $this->_trimZeros(
                            $this->_getOrder()
                                ->getOrderCurrency()
                                ->formatPrecision(
                                    $this->_getReferenceObject()
                                          ->getGrandTotal(),
                                    2,
                                    [],
                                    false
                                )
                        ),
                        'short' => true,
                    ],
                ],
                $this->_prepareOrderFields()
            ),
        ];
    }
}
