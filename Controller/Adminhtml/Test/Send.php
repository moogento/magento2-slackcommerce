<?php
namespace Moogento\SlackCommerce\Controller\Adminhtml\Test;

class Send extends \Magento\Backend\App\Action
{
    /** @var \Moogento\SlackCommerce\Helper\Api */
    public $apiHelper;

    /** @var \Moogento\SlackCommerce\Helper\Config */
    public $configHelper;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Moogento\SlackCommerce\Helper\Api $apiHelper,
        \Moogento\SlackCommerce\Helper\Config $configHelper
    ) {
        $this->apiHelper    = $apiHelper;
        $this->configHelper = $configHelper;
        parent::__construct($context);
    }

    /**
     * Dispatch request
     *
     * @return \Magento\Framework\Controller\ResultInterface|ResponseInterface
     * @throws \Magento\Framework\Exception\NotFoundException
     */
    public function execute()
    {
        try {
            $result = $this->apiHelper->send(
                [
                    'text'     => __(
                        'Moooo! Testing... Daisy 1.. Daisy 2.. Daisy 3..'
                    ),
                    'icon_url' => $this->configHelper->getDefaultIcon(),
                ]
            );
            if ($result === true) {
                $this->messageManager->addSuccess(
                    __(
                        'Check the %1 channel for our test moo, '
                            . 'we enlisted help from Daisy 1-3',
                        $this->configHelper->getDefaultChannel()
                    )
                );
            } else {
                $this->messageManager->addError(
                    __('Oops! I couldn\'t send the test message: %1', $result)
                );
            }
        } catch (\Exception $e) {
            $this->messageManager->addError($e->getMessage());
        }
        $resultRedirect = $this->resultRedirectFactory->create();
        $resultRedirect->setRefererUrl();
        return $resultRedirect;
    }
}
