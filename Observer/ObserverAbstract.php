<?php
namespace Moogento\SlackCommerce\Observer;

use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Moogento\SlackCommerce\Helper\Config;
use Psr\Log\LoggerInterface;
use Moogento\SlackCommerce\Model\QueueFactory;

abstract class ObserverAbstract implements ObserverInterface
{

    /** @var Config */
    public $configHelper;

    /** @var \Magento\Framework\Stdlib\DateTime\DateTime */
    public $dateTime;

    /** @var LoggerInterface */
    public $logger;

    /** @var \Magento\Framework\DB\Adapter\AdapterInterface */
    public $connection;

    /** @var ResourceConnection */
    public $resource;

    /** @var \Magento\Framework\App\Request\Http */
    public $request;

    public $queueFactory;

    public function __construct(
        \Magento\Framework\App\Request\Http $request,
        LoggerInterface $logger,
        ResourceConnection $resource,
        Config $configHelper,
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        QueueFactory $queueFactory
    ) {
        $this->queueFactory = $queueFactory;
        $this->request = $request;
        $this->configHelper = $configHelper;
        $this->logger = $logger;
        $this->resource = $resource;
        $this->connection = $resource->getConnection();
        $this->dateTime = $date;
    }

    /**
     * @param Observer $observer
     *
     * @return void
     */
    abstract public function execute(Observer $observer);
}
