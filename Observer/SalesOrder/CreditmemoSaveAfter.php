<?php
namespace Moogento\SlackCommerce\Observer\SalesOrder;

use Magento\Framework\Event\Observer;
use Moogento\SlackCommerce\Helper\Config;
use Moogento\SlackCommerce\Observer\ObserverAbstract;

class CreditmemoSaveAfter extends ObserverAbstract
{
    /**
     * @param Observer $observer
     *
     * @return void
     */
    public function execute(Observer $observer)
    {
        /** @var \Magento\Sales\Model\Order\Creditmemo $creditmemo */
        $creditmemo = $observer->getEvent()->getCreditmemo();
        if ($creditmemo->getData(Config::NOTIFICATION_FLAG)
            && $this->configHelper->shouldSend(
                \Moogento\SlackCommerce\Model\Queue::KEY_NEW_CREDIT
            )
        ) {
            $creditmemo->setData(Config::NOTIFICATION_FLAG, false);

            $queue = $this->queueFactory->create();
            $queue->setData(
                [
                    'event_key' =>
                        \Moogento\SlackCommerce\Model\Queue::KEY_NEW_CREDIT,
                    'reference_id' => $creditmemo->getId(),
                    'date' => $this->dateTime->gmtDate("Y-m-d H:i:s"),
                ]
            );
            try {
                $queue->save();
            } catch (\Exception $e) {
                $this->logger->warning($e);
            }
        }
    }
}
