<?php
namespace Moogento\SlackCommerce\Model\Notification;

class NewShipment extends NewOrder
{
    protected $_referenceModel = '\Magento\Sales\Model\Order\Shipment';

    protected function _getOrder()
    {
        return $this->_getReferenceObject()->getOrder();
    }

    protected function _prepareText()
    {
        return __('Shipment (Order #%1)', $this->_getOrder()->getIncrementId());
    }

    protected function _getAttachments()
    {
        return [
            'fields' => array_merge(
                [
                    [
                        'title' => __('Total Qty Shipped'),
                        'value' => (float) $this->_getReferenceObject()
                                                ->getTotalQty(),
                        'short' => true,
                    ],
                ],
                $this->_prepareOrderFields()
            ),
        ];
    }
}
