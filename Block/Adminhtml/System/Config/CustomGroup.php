<?php
namespace Moogento\SlackCommerce\Block\Adminhtml\System\Config;

use Magento\Framework\Data\Form\Element\Renderer\RendererInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Widget\Block\Adminhtml\Widget;

/**
 * @method \Magento\Framework\Data\Form\Element\AbstractElement getElement
 * @method CustomGroup setElement
 * @method CustomGroup setScope
 */
class CustomGroup extends Widget implements RendererInterface
{
    /** @var \Moogento\SlackCommerce\Helper\Config */
    protected $_configHelper;

    /** @var  \Magento\Framework\Json\Helper\Data */
    protected $_jsonHelper;

    /** @var \Magento\Backend\Model\Auth\Session */
    protected $_auth;

    /** @var \Magento\Framework\View\Helper\Js */
    protected $_jsHelper;

    /** @var \Magento\Config\Model\Config\Source\Locale\Weekdays */
    protected $_weekdays;

    protected $_list = [];

    public function __construct(
        \Magento\Backend\Block\Widget\Context $context,
        \Magento\Backend\Model\Auth\Session $authSession,
        \Magento\Framework\View\Helper\Js $jsHelper,
        \Moogento\SlackCommerce\Helper\Config $configHelper,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        \Magento\Config\Model\Config\Source\Locale\Weekdays $weekdays,
        array $data = []
    ) {
        $this->_configHelper = $configHelper;
        $this->_jsonHelper = $jsonHelper;
        $this->_weekdays = $weekdays;
        $this->_auth = $authSession;
        $this->_jsHelper = $jsHelper;

        parent::__construct($context, $data);
    }

    /**
     * Render form element as HTML
     *
     * @param \Magento\Framework\Data\Form\Element\AbstractElement $element
     *
     * @return string
     */
    public function render(
        \Magento\Framework\Data\Form\Element\AbstractElement $element
    ) {
        $this->setElement($element);
        return $this->toHtml();
    }

    public function showInheritCheckbox()
    {
        $showInheritCheckbox = false;
        if ($this->_getScope() == ScopeInterface::SCOPE_STORES) {
            $showInheritCheckbox = true;
        } elseif ($this->_getScope() == ScopeInterface::SCOPE_WEBSITES) {
            $showInheritCheckbox = true;
        }

        return $showInheritCheckbox;
    }

    protected function _getScope()
    {
        $scope = $this->getData('scope');
        if ($scope == null) {
            if ($this->_getStoreCode()) {
                $scope = ScopeInterface::SCOPE_STORES;
            } elseif ($this->_getWebsiteCode()) {
                $scope = ScopeInterface::SCOPE_WEBSITES;
            } else {
                $scope = 'default';
            }
            $this->setScope($scope);
        }

        return $scope;
    }

    protected function _getStoreCode()
    {
        return $this->getRequest()->getParam('store', '');
    }

    protected function _getWebsiteCode()
    {
        return $this->getRequest()->getParam('website', '');
    }

    protected function _getInheritLabel()
    {
        $checkboxLabel = '';
        if ($this->_getScope() == ScopeInterface::SCOPE_STORES) {
            $checkboxLabel = __('Use Website');
        } elseif ($this->_getScope() == ScopeInterface::SCOPE_WEBSITES) {
            $checkboxLabel = __('Use Default');
        }

        return $checkboxLabel;
    }

    public function getExtraJs()
    {
        $element = $this->getElement();
        $htmlId  = $element->getHtmlId();
        $output
                 = "require(['prototype'], "
                   . "function(){Fieldset.applyCollapse('{$htmlId}');});";
        return $this->_jsHelper->getScript($output);
    }

    public function isCollapseState()
    {
        $element = $this->getElement();
        if ($element->getExpanded()) {
            return true;
        }

        $extra = $this->_auth->getUser()->getExtra();
        if (isset($extra['configState'][$element->getId()])) {
            return $extra['configState'][$element->getId()];
        }
        return false;
    }
}
