<?php
namespace Moogento\SlackCommerce\Observer;

use Magento\Framework\Event\Observer;

class BackendLoginSuccess extends ObserverAbstract
{
    /**
     * @param Observer $observer
     *
     * @return void
     */
    public function execute(Observer $observer)
    {
        /** @var \Magento\User\Model\User $user */
        $user = $observer->getUser();

        if ($this->configHelper->shouldSend(
            \Moogento\SlackCommerce\Model\Queue::KEY_BACKEND_LOGIN
        )) {

            $queue = $this->queueFactory->create();

            $queue->setData(
                [
                    'event_key' =>
                        \Moogento\SlackCommerce\Model\Queue::KEY_BACKEND_LOGIN,
                    'reference_id' => $user->getId(),
                    'date' => $this->dateTime->gmtDate("Y-m-d H:i:s"),
                ]
            );

            try {
                $queue->save();
            } catch (\Exception $e) {
                $this->logger->warning($e);
            }
        }
    }
}
