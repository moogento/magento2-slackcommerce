<?php
namespace Moogento\SlackCommerce\Helper;

use Magento\Framework\App\Helper\AbstractHelper;

class Data extends AbstractHelper
{

    public $httpClientFactory;

    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Framework\HTTP\ZendClientFactory $httpClientFactory
    ) {
        $this->httpClientFactory = $httpClientFactory;
        parent::__construct($context);
    }

    public function getCountryByIp($ip)
    {
        /** @var  \Magento\Framework\HTTP\ZendClient $client */
        $client = $this->httpClientFactory->create();
        $client->setUri('http://www.geoplugin.net/json.gp?ip=' . $ip);
        $client->setConfig(['maxredirects' => 0, 'timeout' => 30]);
        $client->setMethod(\Zend_Http_Client::GET);
        try {
            $response     = $client->request();
            $responseBody = $response->getBody();
            $ipData       = json_decode($responseBody, true);
            if (isset($ipData['geoplugin_countryName'])) {
                return $ipData['geoplugin_countryName'];
            }
            return false;
        } catch (\Exception $e) {
            return false;
        }
    }
}
