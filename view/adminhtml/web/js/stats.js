define([
    'knockout',
    './ko/bind/switch',
    './ko/bind/color'
], function(ko) {
    'use strict';

    var WeekDay = function(data) {
        var self = this;
        self.value = ko.observable(data.value);
        self.label = ko.observable(data.label);
    };

    var StatsConfig = function(data) {
        var self = this;

        self.stats_send_type = ko.observable(data.stats_send_type);
        self.stats_custom_channel = ko.observable(data.stats_custom_channel);
        self.stats_colorize = ko.observable(+data.stats_colorize);
        self.stats_color = ko.observable(data.stats_color);

        self.qty_orders = ko.observable(+data.qty_orders);
        self.total_revenue = ko.observable(+data.total_revenue);
        self.qty_products = ko.observable(+data.qty_products);
        self.avg_products_order = ko.observable(+data.avg_products_order);
        self.avg_revenue_order = ko.observable(+data.avg_revenue_order);
        self.hour = ko.observable(data.hour);
        self.daily_stats = ko.observable(+data.daily_stats);
        self.day = ko.observable(data.day);
        self.weekly_stats = ko.observable(+data.weekly_stats);

        self.weekdays = ko.observableArray();
        ko.utils.arrayForEach(data.weekdays, function(d) {
            self.weekdays.push(new WeekDay(d));
        });

        self.buildName = function(field, multi) {
            return 'groups[stats][fields][' + field + '][value]' + (multi ? '[]' : '');
        };
        self.getTemplate = function() {
            return 'Moogento_SlackCommerce/stats';
        };
    };

    return function(data) {
        return new StatsConfig(data);
    };
});
